// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

function flag = MY_assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

grand("setsd",0);

// Sort realistic data
e = [
-0.123452 - 0.123454 * %i
-0.123451 + 0.123453 * %i
0.123458 - 0.123459 * %i
0.123456 + 0.123457 * %i
];
// Permute randomly the array
n = size(e,"*");
ip = grand(1,"prm",(1:n)');
x = e(ip);
// Consider less than 4 significant digits
y = assert_sortcomplex(x,1.e-4);
MY_assert_equal ( y , e );
////////////////////////////////////////
// Sort simple data
e = [1 2 3 4];
// Permute randomly the array
n = size(e,"*");
ip = grand(1,"prm",(1:n)');
x = e(ip);
// Use default tolerance
y = assert_sortcomplex(x);
MY_assert_equal ( y , e );
////////////////////////////////////////


// Compare data from bug #415
e = [
-1.9914145
-1.895889
-1.6923826
-1.4815461
-1.1302576
-0.5652256 - 0.0655080 * %i
-0.5652256 + 0.0655080 * %i
0.3354023 - 0.1602902 * %i
0.3354023 + 0.1602902 * %i
1.3468911
1.5040136
1.846668
1.9736772
1.9798866
];
// Permute randomly the array
n = size(e,"*");
ip = grand(1,"prm",(1:n)');
x = e(ip);
// Consider less than 4 significant digits
y = assert_sortcomplex(x,1.e-5);
MY_assert_equal ( y , e );


// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// <-- Non-regression test for bug 856 -->
//
// <-- Bugzilla URL -->
// http://forge.scilab.org/index.php/p/apifun/issues/856/
//
// <-- Short Description -->
//   assert_checkfilesequal fails when \n come into play

function flag = MY_assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

instr="assert_checkfilesequal(""C:\mypath\normaldir\file1.txt"",""C:\mypath\normaldir\file2.txt"")";
ierr=execstr(instr,"errcatch");
MY_assert_equal ( ierr , 10000 );
errmsg=lasterror();
MY_assert_equal ( errmsg , "assert_checkfilesequal: The file C:\mypathormaldir\file1.txt does not exist." );

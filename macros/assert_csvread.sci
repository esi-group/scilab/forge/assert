// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function A = assert_csvread ( varargin )
    // Read data from a file or a string.
    //
    // Calling Sequence
    //   A = assert_csvread ( srcstring )
    //   A = assert_csvread ( srcstring , sep )
    //   A = assert_csvread ( srcstring , sep , dec )
    //   A = assert_csvread ( srcstring , sep , dec , comm )
    //   A = assert_csvread ( srcstring , sep , dec , comm , srctype )
    //   A = assert_csvread ( srcstring , sep , dec , comm , srctype , replacemap )
    //   A = assert_csvread ( srcstring , sep , dec , comm , srctype , replacemap , output )
    //   A = assert_csvread ( srcstring , sep , dec , comm , srctype , replacemap , output , irange )
    //
    // Parameters
    //   srcstring : a matrix of strings, the file name (default) or the data. See strtype below.
    //   sep : a 1-by-1 matrix of strings, the column separator (default sep=",").
    //   dec : a 1-by-1 matrix of strings, the decimal mark (default dec="."). The decimal mak can be equal to "." or ",". Any other value generates an error.
    //   comm : a 1-by-1 matrix of strings, the regexp defining the comment lines (default comm="/#(.*)/"). Any line in the file matching this regexp is ignored. The default regexp ensures that any line which first caracter is a # is ignored.
    //   srctype : a 1-by-1 matrix of booleans (default srctype=%f). If srctype is %f, then srcstring must be a 1-by-1 matrix of strings, representing a file which can be read from disk. If srctype is %t, then srcstring must be a matrix of strings, with 1 column only, and is considered as the data itself.
    //   replacemap : a n-by-2 matrix of strings, the replace map (default replacemap=["Inf","%inf";"Nan","%nan";"I","%i"]). The first column contains the source strings, while the second column contains the target strings.
    //   output : a 1-by-1 matrix of floating point integers, the type of output (default output=1 for a matrix of strings). If output=1, the output A is a matrix of strings. If output=2, the output A is a matrix of doubles.
    //   irange : a 1-by-4 matrix of floating point integers, the range of rows and columns to read (default irange=[1 1 %inf %inf]). The values are irange = [R1 C1 R2 C2] where (R1,C1) is the upper left corner of the data to be read and (R2,C2) is the lower right corner. We must have 1<=R1<=R2 and 1<=C1<=C2.
    //   A : a matrix of strings or a matrix of doubles
    //
    // Description
    //   Reads the data from a file.
    //
    //   The replacemap argument allows to customize the management of special values such as 
    //   Infinities, Nans, complex numbers, etc...
    //   The replacement is done in the order specified by the map. 
    //   Therefore, the order matters in the map.
    //   For example, if poorly ordered, the replacement of "I" by "%i" may interact 
    //   with the replacement of "Inf" by "%inf", because the "I" in "Inf" may be 
    //   processed earlier.
    //   To avoid this, longer strings should be located earlier in the map.
    //
    //   The current function also manages the reading with more flexibility than 
    //   the <literal>read_csv</literal> function.
    //   Indeed, the <literal>read_csv</literal> function does not allow to configure the
    //   <literal>replacemap</literal>.
    //   Moreover, the <literal>read_csv</literal> function does not allow to ignore the comments 
    //   in the data file.
    //   This is a problem if the file contains for example a licence header.
    //   Moreover, the <literal>read_csv</literal> function does not allow to read into 
    //   a string: the data must be stored in a file.
    //   This is a problem if the user has to perform a pre-processing of the data: 
    //   in this case, the current function allows to read directly in the data.
    //
    // This function fills empty delimited fields with zero.
    //
    // Empty lines (i.e. with only blank spaces, or tabs) are ignored.
    //
    // Examples
    // // Define a matrix of strings
    // Astr = [
    // "1" "8" "15" "22" "29" "36" "43" "50"
    // "2" "9" "16" "23" "30" "37" "44" "51"
    // "3" "10" "17" "6+3*I" "31" "38" "45" "52"
    // "4" "11" "18" "25" "32" "39" "46" "53"
    // "5" "12" "19" "26" "33" "40" "47" "54"
    // "6" "13" "20" "27" "34" "41" "48" "55"
    // "+0" "-0" "Inf" "-Inf" "Nan" "1.D+308" "1.e-308" "1.e-323"
    // ];
    //
    // // Create a file with some data separated with commas    
    // srcstring = fullfile(TMPDIR , 'foo.csv');
    // sep = ",";
    // fd = mopen(srcstring,'wt');
    // for i = 1 : size(Astr,"r")
    //     mfprintf(fd,"%s\n",strcat(Astr(i,:),sep));
    // end
    // mclose(fd);
    // // To see the file : edit(srcstring)
    //
    // // Read this file
    // Bstr = assert_csvread ( srcstring )
    // // To convert this data into a double :
    // Bdbl = evstr(Bstr)
    //
    // // Create a file with a particular separator: here ";"
    // srcstring = fullfile(TMPDIR , 'foo.csv');
    // sep = ";";
    // fd = mopen(srcstring,'wt');
    // for i = 1 : size(Astr,"r")
    //     mfprintf(fd,"%s\n",strcat(Astr(i,:),sep));
    // end
    // mclose(fd);
    // //
    // // Read the file and customize the separator
    // assert_csvread ( srcstring , sep )
    //
    // // Create a file with comments
    // srcstring = fullfile(TMPDIR , 'foo.csv');
    // sep = ",";
    // fd = mopen(srcstring,'wt');
    // mfprintf(fd,"#This is a comment\n");
    // for i = 1 : size(Astr,"r")
    //     mfprintf(fd,"%s\n",strcat(Astr(i,:),sep));
    //     mfprintf(fd,"# This is another comment\n");
    // end
    // mfprintf(fd,"# This is a final comment\n");
    // mclose(fd);
    // //
    // // Read the file and ignore the comments
    // assert_csvread ( srcstring )
    // // Customize the comment regular expression
    // assert_csvread ( srcstring , [] , [] , "/#(.*)/" )
    //
    //
    // // Create a data file with particular Infinities and complex numbers
    // Astr = [
    // "6+3*I" "13-7*I" "20+4*I" "27-1.5*I" "34+3.14*I" "41-3*I" "48+3*I" "55-7*I"
    // "+0" "-0" "Infinity" "-Infinity" "Nan" "1.D+308" "1.e-308" "1.e-323"
    // ];
    // expected = [
    // "6+3*%i" "13-7*%i" "20+4*%i" "27-1.5*%i" "34+3.14*%i" "41-3*%i" "48+3*%i" "55-7*%i"
    // "+0" "-0" "%inf" "-%inf" "%nan" "1.D+308" "1.e-308" "1.e-323"
    // ];
    // filename = fullfile(TMPDIR , "foo.csv");
    // sep = ",";
    // fd = mopen(filename,'wt');
    // for i = 1 : size(Astr,"r")
    //     mfprintf(fd,"%s\n",strcat(Astr(i,:),sep));
    // end
    // mclose(fd);
    // //
    // // Customize the replace map.
    // replacemap=["Nan","%nan";"Infinity","%inf";"I","%i"];
    // assert_csvread ( filename , [] , [] , [] , [] , replacemap )
    //
    // // Configure the decimal mark.
    // Atext = [
    // " 1,000000000D+00; 0,000000000D+00; 2,000000000D+02;             Inf; 0,000000000D+00";  
    // " 1,000000000D+00; 1,00000000D-300; 2,000000000D+02;             Inf; 0,000000000D+00";   
    // " 1,000000000D+00; 1,00000000D-200; 2,000000000D+02; 3,15000000D+300; 1,020000000D+02";   
    // " 9,999999999D-01; 1,00000000D-100; 2,000000000D+02; 2,960000000D+02; 1,170000000D+02";   
    // " 1,000000000D+00;             Inf;            -Inf;             Nan; 0,000000000D+00"
    // ];
    // assert_csvread ( Atext , ";" , "," , [] , %t )
    //
    // // Check that datasetread and datasetwrite are inverse.
    // A = rand(3,4)+rand(3,4)*%i
    // Astr = assert_csvwrite(A);
    // Bstr = assert_csvread(Astr,[],[],[],%t);
    // B = evstr(Bstr);
    // and(A==B) 
    //
    // // Read a data with an inconsistent number of columns
    // Atxt=[
    // "40 5 30 1.6 0.2 1.2"
    // "15 25 35 0.6 1 1.4"
    // "20 45 10 0.8 1.8 0.4"
    // ""
    // "2.6667 0.33333 2"
    // "1 1.6667 2.3333"
    // "1.3333 3 0.66667"
    // ];
    // A = assert_csvread ( Atxt," ",[],[],%t )
    // // Read a file with an unconsistent number of columns
    // Atxt=[
    // "2.6667 0.33333 2"
    // "1 1.6667 2.3333"
    // "1.3333 3 0.66667"
    // ""
    // "40 5 30 1.6 0.2 1.2"
    // "15 25 35 0.6 1 1.4"
    // "20 45 10 0.8 1.8 0.4"
    // ];
    // A = assert_csvread ( Atxt," ",[],[],%t )
    //
    // // Read data and convert into doubles
    // Astr = [
    // "1,8,15,22,29,36,43,50"
    // "2,9,16,23,30,37,44,51"
    // "3,10,17,6+3*I,31,38,45,52"
    // "4,11,18,25,32,39,46,53"
    // "5,12,19,26,33,40,47,54"
    // "6,13,20,27,34,41,48,55"
    // "+0,-0,Inf,-Inf,Nan,1.D+308,1.e-308,1.e-323"
    // ];
    // A = assert_csvread ( Astr,[],[],[],%t,[],2 )
    //
    // // Read only rows/columns in range
    // Astr = [
    // "1,8,15,22,29,36,43,50"
    // "2,9,16,23,30,37,44,51"
    // "3,10,17,6+3*I,31,38,45,52"
    // "4,11,18,25,32,39,46,53"
    // "5,12,19,26,33,40,47,54"
    // "6,13,20,27,34,41,48,55"
    // "+0,-0,Inf,-Inf,Nan,1.D+308,1.e-308,1.e-323"
    // ];
    // A = assert_csvread ( Astr,[],[],[],%t,[],[],[2 3 5 6] );
    //
    // // The irange argument allows to read large datasets.
    // // In the following example, we read only the last line of a large dataset.
    // n=50;
    // M = ones(n, n);
    // filename=fullfile(TMPDIR,"foo.csv");
    // assert_csvwrite(M,filename);
    // assert_csvread(filename);
    // Mlast = assert_csvread ( filename , [] , [] , [] , [] , [] , [] , [n 1 n n] );
    //
    // //
    // // We create a "large" csv file with 10 000 rows and 3 columns.
    // filename=fullfile(TMPDIR,"prices.csv");
    // fd = mopen(filename,"w");
    // for i= 1:10000
    //   mputl("02/05/10 00:00:02,1.32453,1.32491",fd);
    // end
    // mclose(fd);
    // //
    // // We read it by blocks of 1234 rows and end the read when the 
    // // end of the file is reached.
    // blocksize = 1234;
    // C1 = 1;
    // C2 = 3;
    // iblock = 1
    // while (%t)
    //   R1 = (iblock-1) * blocksize + 1;
    //   R2 = blocksize + R1-1;
    //   irange = [R1 C1 R2 C2];
    //   M=assert_csvread(filename , [] , [] , [] , [] , [] , [] , irange );
    //   nrows = size(M,"r");
    //   disp([iblock R1 R2 nrows])
    //   if ( nrows < blocksize ) then
    //     break
    //   end
    //   iblock = iblock + 1;
    // end
    //
    // Authors
    //   Michael Baudin, Digiteo, 2010
    //

    [lhs,rhs]=argn()
    if ( and ( rhs <> [1 2 3 4 5 6 7 8] ) ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected number of input arguments : %d provided while %d to %d are expected.") , "assert_csvread" , rhs , 1 , 6 )
        error(errmsg)
    end
    //
    // Get arguments
    srcstring = varargin(1)
    sep = argindefault ( rhs , varargin , 2 , "," )
    dec = argindefault ( rhs , varargin , 3 , [] )
    comm = argindefault ( rhs , varargin , 4 , "/#(.*)/" )
    srctype = argindefault ( rhs , varargin , 5 , %f )
    replacemap = argindefault ( rhs , varargin , 6 , ["Inf","%inf";"Nan","%nan";"I","%i"] )
    output = argindefault ( rhs , varargin , 7 , 1 )
    irange = argindefault ( rhs , varargin , 8 , [] )
    //
    // Check types of variables
    if ( typeof(srcstring) <> "string" ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_csvread" , 1 , "srcstring" , typeof(srcstring) , "string" )
        error(errmsg)
    end
    if ( typeof(sep) <> "string" ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_csvread" , 2 , "sep" , typeof(sep) , "string" )
        error(errmsg)
    end
    if ( dec <> [] ) then
        if ( typeof(dec) <> "string" ) then
            errmsg = sprintf ( gettext ( "%s: Expected type ""%s"" for input argument %s #%d, but got %s instead.") , "assert_csvread" , "string" , "dec" , 3 , typeof(dec) )
            error(errmsg)
        end
    end
    if ( typeof(comm) <> "string" ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_csvread" , 4 , "comm" , typeof(comm) , "string" )
        error(errmsg)
    end
    if ( typeof(srctype) <> "boolean" ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_csvread" , 5 , "srctype" , typeof(srctype) , "boolean" )
        error(errmsg)
    end
    if ( typeof(replacemap) <> "string" ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_csvread" , 6 , "replacemap" , typeof(replacemap) , "string" )
        error(errmsg)
    end
    if ( typeof(output) <> "constant" ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_csvread" , 7 , "output" , typeof(output) , "constant" )
        error(errmsg)
    end
    if ( typeof(irange) <> "constant" ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_csvread" , 8 , "irange" , typeof(irange) , "constant" )
        error(errmsg)
    end
    //
    // Check size of variables
    if ( srctype ) then
        if ( size(srcstring,"c") <> 1 ) then
            errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has %d columns while %d is expected.") , "assert_csvread" , 1 , "srcstring" , size(srcstring,"c") , 1 )
            error(errmsg)
        end
    else
        if ( size(srcstring,"*") <> 1 ) then
            errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has size %d while %d is expected.") , "assert_csvread" , 1 , "srcstring" , size(srcstring,"*") , 1 )
            error(errmsg)
        end
    end
    if ( size(sep,"*") <> 1 ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has size %d while %d is expected.") , "assert_csvread" , 2 , "sep" , size(sep,"*") , 1 )
        error(errmsg)
    end
    if ( dec <> [] ) then
        if ( size(dec,"*") <> 1 ) then
            errmsg = sprintf ( gettext ( "%s: Wrong size for input argument #%d: %s: Expected size %d, but got %d instead.") , "assert_csvread" , 3 , "dec" , 1 , size(dec,"*") )
            error(errmsg)
        end
    end
    if ( size(comm,"*") <> 1 ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has size %d while %d is expected.") , "assert_csvread" , 4 , "comm" , size(comm,"*") , 1 )
        error(errmsg)
    end
    if ( size(srctype,"*") <> 1 ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has size %d while %d is expected.") , "assert_csvread" , 5 , "srctype" , size(srctype,"*") , 1 )
        error(errmsg)
    end
    if ( size(replacemap,"c") <> 2 ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has size %d while %d is expected.") , "assert_csvread" , 6 , "replacemap" , size(replacemap,"c") , 1 )
        error(errmsg)
    end
    if ( size(output,"*") <> 1 ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has size %d while %d is expected.") , "assert_csvread" , 7 , "output" , size(output,"*") , 1 )
        error(errmsg)
    end
    if ( irange <> [] ) then
        if ( size(irange) <> [1 4] ) then
            errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has size [%d,%d] while [%d,%d] is expected.") , "assert_csvread" , 8 , "irange" , size(irange,"r") , size(irange,"c") , 1 , 4 )
            error(errmsg)
        end
    end
    //
    // Check content of variables
    if ( ~srctype & fileinfo ( srcstring ) == [] ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected input argument #%d : file %s does not exist.") , "assert_csvread" , 1 , srcstring )
        error(errmsg)
    end
    if ( dec <> [] ) then
        if ( and(dec <> ['.',',']) ) then
            error(msprintf(gettext("%s: Wrong value for input argument #%d: %s: ''%s'' or ''%s'' expected.\n"), "assert_csvread", 3 , "dec" , ".", ","));
        end
    end
    if ( round(output) <> output ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected input argument #%d : %s is expected to an integer, but %.e has a fractional part.") , "assert_csvread" , 7 , "output" , output )
        error(errmsg)
    end
    if ( and(output <> [1 2]) ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected input argument #%d : %s is equal to %d, but from %d to %d is expected.") , "assert_csvread" , 7 , "output" , output , 1 , 2 )
        error(errmsg)
    end
    if ( irange <> [] ) then
        if ( or(irange <=0) ) then
            errmsg = sprintf ( gettext ( "%s: Unexpected input argument #%d : one entry of %s is lower than %d.") , "assert_csvread" , 8 , "irange" , 1 )
            error(errmsg)
        end
    end
    //
    // Set the index range
    if ( irange == [] ) then
        R1 = 1
        C1 = 1
        R2 = %inf
        C2 = %inf
    else
        R1 = irange(1)
        C1 = irange(2)
        R2 = irange(3)
        C2 = irange(4)
    end
    if ( R2<R1 ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected input argument #%d : %s : %s is lower than %s.") , "assert_csvread" , 8 , "irange" , msprintf("R1=%d",R1) , msprintf("R2=%d",R2) )
        error(errmsg)
    end
    if ( C2<C1 ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected input argument #%d : %s : %s is lower than %s.") , "assert_csvread" , 8 , "irange" , msprintf("C1=%d",C1) , msprintf("C2=%d",C2) )
        error(errmsg)
    end
    //
    // Read the file, if required
    if ( srctype ) then
        srcnrows = size(srcstring,"r")
        R2 = min(srcnrows,R2)
        datastring = srcstring(R1:R2)
    else
        [fd,err]= mopen(srcstring,"r")
        if ( err <> 0 ) then
            errmsg = sprintf ( gettext ( "%s: Unable to open file %s") , "assert_csvread" , filename )
            error(errmsg)
        end
        ignore = mgetl(fd,R1-1)
        datastring = mgetl(srcstring,R2-R1+1)
        err=mclose(fd)
        if ( err <> 0 ) then
            errmsg = sprintf ( gettext ( "%s: Unable to close file %s") , "assert_csvread" , filename )
            error(errmsg)
        end
    end
    //
    // Remove comment lines.
    k = grep ( datastring , comm , "r" );
    datastring(k)=[];
    //
    // Remove empty lines
    datastring = stripblanks(datastring)
    k = find(datastring=="")
    datastring(k)=[]
    //
    // Read one line after the other, using the separators.
    nrows = size(datastring,"r")
    A = []
    first = %t
    for i = 1 : nrows
        line = datastring(i)
        //
        // Search the separator in the line.
        // This cannot be vectorized, because strindex in Scilab v5 is not
        // vectorized.
        // See http://bugzilla.scilab.org/show_bug.cgi?id=8683
        K = strindex(line, sep)
        //
        // Create a row, splitting on each separator
        if ( K == [] ) then
            row = line
        else
            nsep = size(K,"*")
            linesplt = strsplit ( line , K )
            linesplt = strsubst ( linesplt , sep , "" )
            row = linesplt'
        end
        //
        // Discard unnecessary columns
        Cend=min(size(row,"c"),C2)
        row=row(C1:Cend)
        //
        // Update the number of columns, if necessary.
        if (first) then
            first = %f
        else
            if ( size(row,2) > size(A,2) ) then
                //
                // If there are more columns in row than in A,
                // add one empty column in A.
                A(1:$,size(A,2)+1:size(row,2)) = "0"
            elseif ( size(row,2) < size(A,2) ) then
                //
                // If there are more columns in A than in row,
                // add one empty column in row.
                row(1, size(row,2)+1:size(A,2)) = "0"
            end
        end
        //
        // Update the matrix of strings.
        A = [A;row]
    end
    //
    // Replace special strings.
    // Let this at the end to let vectorization take place.
    for k = 1 : size(replacemap,"r")
        A = strsubst(A,replacemap(k,1),replacemap(k,2))
    end
    //
    // Replace the decimal mark, if necessary.
    // Let this at the end to let vectorization take place.
    if ( dec <> [] ) then
        if ( dec <> "." ) then
            A = strsubst(A,dec,".")
        end
    end
    //
    // Remove the blanks, if any.
    A = stripblanks(A)
    //
    // Convert into double, if required
    if ( output == 2 ) then
        [A,ierr] = evstr(A)
        if ( ierr <> 0 ) then
            lamsg = lasterror()
            errmsg = sprintf ( gettext ( "%s: Unable to convert into double: %s") , "assert_csvread" , lamsg )
            error(errmsg)
        end
    end
endfunction
function argin = argindefault ( rhs , vararglist , ivar , default )
    // Returns the value of the input argument #ivar.
    // If this argument was not provided, or was equal to the 
    // empty matrix, returns the default value.
    if ( rhs < ivar ) then
        argin = default
    else
        if ( vararglist(ivar) <> [] ) then
            argin = vararglist(ivar)
        else
            argin = default
        end
    end
endfunction


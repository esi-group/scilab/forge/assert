// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function strmat = assert_csvwrite ( varargin )
    //   Write data into a file or a string.
    //
    // Calling Sequence
    //   strmat = assert_csvwrite ( A )
    //   strmat = assert_csvwrite ( A , filename )
    //   strmat = assert_csvwrite ( A , filename , sep )
    //   strmat = assert_csvwrite ( A , filename , sep , dec )
    //   strmat = assert_csvwrite ( A , filename , sep , dec , digits )
    //   strmat = assert_csvwrite ( A , filename , sep , dec , digits , replacemap )
    //   strmat = assert_csvwrite ( A , filename , sep , dec , digits , replacemap , header )
    // 
    // Parameters
    //   A : a m-by-n matrix of doubles, the A of values.
    //   filename : a 1-by-1 matrix of strings, the name of the file (default filename=[]). If filename is the empty matrix [], the file is not written.
    //   sep : a 1-by-1 matrix of strings, the separator between the columns (default sep=",").
    //   dec : a 1-by-1 matrix of strings, the decimal mark (default dec="."). The decimal mak can be equal to "." or ",". Any other value generates an error.
    //   digits : a 1-by-1 matrix of floating point integers or string, the precision of the numbers (default digits=17). If digits is a matrix of doubles, it must be the number of digits to display. If digits is a matrix of strings, it must be the format used by msprintf to format the doubles. In this case, the first character must be %. The recommended format is digits="%.17e".
    //   replacemap : a 3-by-2 matrix of strings, the replace map (default replacemap=["%inf","Inf";"%nan","Nan";"%i","I"]). The first column contains the source strings, while the second column contains the target strings. Each of "%inf", "%nan" and "%i" must be contained in replacemap.
    //   header : a n-by-1 matrix of strings, the header (default header = [], meaning no header at all). This header will be inserted at the begining of the csv file.
    //   strmat : a n-by-1 matrix of strings, the formatted A.
    //
    // Description
    //   Prints A with a given number of digits. 
    //
    //   Any optional input argument equal to the empty matrix is replaced by its default value.
    //
    //   The goal of this function is to provide a portable way of creating tables of 
    //   data, for creation of unit tests.
    //
    //   The default number of digits, i.e. digits=17, ensures that a write-read cycle 
    //   is error-free.
    //   In other words, we can use datasetwrite, then datasetread: the data is represented by 
    //   the same doubles.
    //
    //   We do not use the <literal>msprintf</literal> function, because this function produces a different 
    //   number of digits in the exponent on Windows and on Linux.
    //
    //   This function allows to display values in a more numerically consistent way 
    //   than the built-in <literal>format</literal> and <literal>disp</literal> functions.
    //   For example, the number of digits in the <literal>format</literal> function does not correspond 
    //   to the number of digits after the decimal point.
    //   The current function manages this transparently for the user.
    //
    //   The current function manages the printing more consistently than 
    //   the <literal>sci2exp</literal> function, which does not manage the number of digits.
    //
    //   The current function manages the printing more consistently than 
    //   the <literal>write_csv</literal> function, which does not allow to configure the 
    //   <literal>replacemap</literal>.
    //   Moreover, the <literal>write_csv</literal> function does not allow to access to the 
    //   produced string, in the sense that the data file is forced to be written.
    //   In the case where the user has an additionnal update to do to the produced text, 
    //   the current function allows to do it, while it is not possible with <literal>write_csv</literal>.
    //   Finally, the <literal>write_csv</literal> function does not allow to 
    //   configure the number of printed digits.
    //
    //   Be warned that using the comma "," both as a decimal mark and 
    //   as a separator will lead to an inconsistent data file.
    //
    //   Notice that the default separator is ",", while write_csv is the tab.
    //   Notice that the default decimal mark is ".", while write_csv is the ",".
    //
    // Examples
    // A = [
    //  1 0 200 %inf 0
    //  1 1.e-300 200 %inf 0
    //  9.99999999999990010e-001 9.99999999999999980e-201 200 3.15e300 102
    //  9.99999999899999990e-001 1.e-100 200 296 117
    //  1 %inf -%inf %nan 0
    // ];
    // strmat = assert_csvwrite ( A )
    //
    // // Write into a file
    // filename=fullfile(TMPDIR,"foo.csv");
    // assert_csvwrite ( A , filename );
    // edit(filename)
    // 
    //
    // // Customize the separator
    // strmat = assert_csvwrite ( A , [] , ";" )
    // // Customize the decimal mark
    // strmat = assert_csvwrite ( A , [] , ";" , "," )
    // // Customize the number of digits
    // strmat = assert_csvwrite ( A , [] , [] , [] , 8 )
    //
    // // Create a particular matrix
    // A = [
    // 1+6*%i  , 3-4*%i  , 5-2*%i , 1.e-300
    // -2-5*%i , -4+3*%i , 6+1*%i , 1.e300
    // %inf    , -%inf   , %nan   , 1.e-323
    // ];
    // //
    // // Use the replace map to configure the way %inf, %nan and %i are printed
    // replacemap = ["%inf","infinity";"%nan","notanumber";"%i","i"];
    // strmat = assert_csvwrite ( A , [] , [] , [] , 8 , replacemap)
    //
    // // Configure the format
    // assert_csvwrite ( A , [] , [] , [] , "%.17e" )
    // assert_csvwrite ( A , [] , [] , [] , "%.5e" )
    //
    // // Configure the header
    // header = [
    // "# Copyright (C) 2012 - Foo"
    // "# Do what you want with this file."
    // ];
    // strmat = assert_csvwrite ( A , [] , [] , [] , [] , [] , header )
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010

    [lhs,rhs]=argn()
    if ( and ( rhs <> [ 1 2 3 4 5 6 7] ) ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected number of input arguments : %d provided while %d to %d are expected.") , "assert_csvwrite" , rhs , 1 , 7 )
        error(errmsg)
    end
    //
    // Read arguments
    A = varargin(1)
    filename = argindefault ( rhs , varargin , 2 , [] )
    sep = argindefault ( rhs , varargin , 3 , "," )
    dec = argindefault ( rhs , varargin , 4 , [] )
    digits = argindefault ( rhs , varargin , 5 , 17 )
    replacemap = argindefault ( rhs , varargin , 6 , ["%inf","Inf";"%nan","Nan";"%i","I"] )
    header = argindefault ( rhs , varargin , 7 , [] )
    //
    // Check types of variables
    if ( typeof(A) <> "constant" ) then
        errmsg = sprintf ( gettext ( "%s: Expected type ""%s"" for input argument %s #%d, but got %s instead.") , "assert_csvwrite" , "constant" , "A" , 1 , typeof(A) )
        error(errmsg)
    end
    if ( filename <> [] ) then
        if ( typeof(filename) <> "string" ) then
            errmsg = sprintf ( gettext ( "%s: Expected type ""%s"" for input argument %s #%d, but got %s instead.") , "assert_csvwrite" , "string" , "filename" , 2 , typeof(filename) )
            error(errmsg)
        end
    end
    if ( typeof(sep) <> "string" ) then
        errmsg = sprintf ( gettext ( "%s: Expected type ""%s"" for input argument %s #%d, but got %s instead.") , "assert_csvwrite" , "string" , "sep" , 3 , typeof(sep) )
        error(errmsg)
    end
    if ( dec <> [] ) then
        if ( typeof(dec) <> "string" ) then
            errmsg = sprintf ( gettext ( "%s: Expected type ""%s"" for input argument %s #%d, but got %s instead.") , "assert_csvwrite" , "string" , "dec" , 4 , typeof(dec) )
            error(errmsg)
        end
    end
    if ( and(typeof(digits) <> ["constant" "string"] ) ) then
        errmsg = sprintf ( gettext ( "%s: Expected type ""%s"" or ""%s"" for input argument %s #%d, but got %s instead.") , "assert_csvwrite" , "constant" , "string" , "digits" , 5 , typeof(digits) )
        error(errmsg)
    end
    if ( typeof(replacemap) <> "string" ) then
        errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_csvwrite" , 6 , "replacemap" , typeof(replacemap) , "string" )
        error(errmsg)
    end
    if ( header <> [] ) then
        if ( typeof(header) <> "string" ) then
            errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_csvwrite" , 7 , "header" , typeof(header) , "string" )
            error(errmsg)
        end
    end
    //
    // Check sizes of variables
    if ( filename <> [] ) then
        if ( size(filename,"*") <> 1 ) then
            errmsg = sprintf ( gettext ( "%s: Wrong size for input argument #%d: %s: Expected size %d, but got %d instead.") , "assert_csvwrite" , 2 , "filename" , 1 , size(filename,"*") )
            error(errmsg)
        end
    end
    if ( size(sep,"*") <> 1 ) then
        errmsg = sprintf ( gettext ( "%s: Wrong size for input argument #%d: %s: Expected size %d, but got %d instead.") , "assert_csvwrite" , 3 , "sep" , 1 , size(sep,"*") )
        error(errmsg)
    end
    if ( dec <> [] ) then
        if ( size(dec,"*") <> 1 ) then
            errmsg = sprintf ( gettext ( "%s: Wrong size for input argument #%d: %s: Expected size %d, but got %d instead.") , "assert_csvwrite" , 4 , "dec" , 1 , size(dec,"*") )
            error(errmsg)
        end
    end
    if ( size(digits,"*") <> 1 ) then
        errmsg = sprintf ( gettext ( "%s: Wrong size for input argument #%d: %s: Expected size %d, but got %d instead.") , "assert_csvwrite" , 5 , "digits" , 1 , size(digits,"*") )
        error(errmsg)
    end
    if ( size(replacemap,"c") <> 2 ) then
        errmsg = sprintf ( gettext ( "%s: Wrong size of input argument #%d : variable %s has size %d while %d is expected.") , "assert_csvwrite" , 6 , "replacemap" , size(replacemap,"c") , 1 )
        error(errmsg)
    end
    if ( header <> [] ) then
        if ( size(header,"c") <> 1 ) then
            errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has size %d while %d is expected.") , "assert_csvwrite" , 7 , "header" , size(header,"c") , 1 )
            error(errmsg)
        end
    end
    //
    // Check content of variables
    if ( typeof(digits)=="constant" ) then
        if ( digits<1 | digits>17 ) then
            errmsg = sprintf ( gettext ( "%s: Wrong value for input argument #%d: %s: Expected from %d to %d, but got %d instead.") , "assert_csvwrite" , 3 , "digits" , 1 , 17 , digits )
            error(errmsg)
        end
        if ( round(digits)<>digits ) then
            errmsg = sprintf ( gettext ( "%s: Wrong value for input argument #%d: %s: Expected floating point integer, but got %f instead.") , "assert_csvwrite" , 3 , "digits" , digits )
            error(errmsg)
        end
    else
        if ( part(digits,1) <> "%" ) then
            errmsg = sprintf ( gettext ( "%s: Wrong value for input argument #%d: %s: Expected %s as first character, but got %s instead.") , "assert_csvwrite" , 3 , "digits" , "%" , part(digits,1) )
            error(errmsg)
        end
    end
    if ( dec <> [] ) then
        if ( and(dec <> ['.',',']) ) then
            error(msprintf(gettext("%s: Wrong value for input argument #%d: %s: ''%s'' or ''%s'' expected.\n"), "assert_csvwrite", 4, "dec" , ".", ","));
        end
    end
    //
    // Proceed...
    // Backup the format
    nrows = size(A,"r")
    ncols = size(A,"c")
    //
    // Prepare the formats
    // fmtExcep : the format for positive exceptionnal numbers: +%inf, %nan, -%inf
    // fmtNegFloats : the format for negative regular numbers: -1.e-10, -1.e-300, etc...
    // fmtPosFloats : the format for positive regular numbers: 1.e-10, 1.e-300, etc...
    if ( typeof(digits)=="string" ) then
        fmtExcep = "%s"
        fmtNegFloats = digits
        fmtPosFloats = digits
    else
        fmtExcep = " %" + msprintf("%d",digits+7) + "s"
        fmtNegFloats = "%." + msprintf("%d",digits) + "e"
        fmtPosFloats = " "+fmtNegFloats
    end
    //
    // See if A is complex
    irA = isreal(A)
    //
    // Search the strings for %i, %inf, %nan
    percentiInd = find(replacemap(:,1)=="%i")
    if ( percentiInd == [] ) then
        errmsg = sprintf ( gettext ( "%s: Wrong value for input argument #%d: %s: The string %s was not found, but is mandatory.") , "assert_csvwrite" , 5 , "replacemap" , "%i" )
        error(errmsg)
    end
    percentiStr = replacemap(percentiInd,2)
    //
    infInd = find(replacemap(:,1)=="%inf")
    if ( infInd == [] ) then
        errmsg = sprintf ( gettext ( "%s: Wrong value for input argument #%d: %s: The string %s was not found, but is mandatory.") , "assert_csvwrite" , 5 , "replacemap" , "%inf" )
        error(errmsg)
    end
    infStr = replacemap(infInd,2)
    //
    nanInd = find(replacemap(:,1)=="%nan")
    if ( nanInd == [] ) then
        errmsg = sprintf ( gettext ( "%s: Wrong value for input argument #%d: %s: The string %s was not found, but is mandatory.") , "assert_csvwrite" , 5 , "replacemap" , "%nan" )
        error(errmsg)
    end
    nanStr = replacemap(nanInd,2)
    //
    // Initialize the string to write
    if ( header <> [] ) then
        strmat = header
    else
        strmat = []
    end
    //
    // Fill the string to write
    if ( irA ) then
        strA = formatdataMatrix ( A , fmtExcep , fmtPosFloats , fmtNegFloats , infStr , nanStr )
    else
        strRe = formatdataMatrix ( real(A) , fmtExcep , fmtPosFloats , fmtNegFloats , infStr , nanStr )
        strIm = formatdataMatrix ( imag(A) , fmtExcep , fmtPosFloats , fmtNegFloats , infStr , nanStr )
        strA = strRe + "+" + percentiStr + "*" + strIm
    end
    // Insert the separator
    strmat = [strmat;strcat(strA,sep,"c")]
    //
    // Manage the decimal mark
    if ( dec <> [] ) then
        if ( dec <> "." ) then
            strmat = strsubst(strmat,".",",");
        end
    end
    //
    // Write the file, if required
    if ( filename <> [] ) then
        r = mputl(strmat,filename);
        if ( ~r ) then
            errmsg = sprintf ( gettext ( "%s: Unable to write the file %s.") , "assert_csvwrite" , filename )
            error(errmsg)
        end
    end
endfunction
function str = formatdataMatrix ( A , fmtExcep , fmtPosFloats , fmtNegFloats , infStr , nanStr )
    //
    // Format the real floating point matrix A.
    //
    // Parameters
    // A : a matrix of real doubles, the data to format
    // fmtExcep : a 1-by-1 matrix of strings, the format for positive exceptionnal numbers: +%inf, %nan
    // fmtNegFloats : a 1-by-1 matrix of strings, the format for negative regular numbers: -1.e-10, -1.e-300, etc...
    // fmtPosFloats : a 1-by-1 matrix of strings, the format for positive regular numbers: 1.e-10, 1.e-300, etc...
    // infStr : a 1-by-1 matrix of strings, the string for infinity
    // nanStr : a 1-by-1 matrix of strings, the string for nan


    nr = size(A,"r")
    nc = size(A,"c")
    n=nr*nc
    // Convert A into column
    A = matrix(A,nr*nc)
    // Initialize the output
    str(1:n)=""
    [kpinf , kninf , knan , kreg , xreg] = infnanindices ( A )
    if ( kpinf <> [] ) then
        str(kpinf) = msprintf(fmtExcep,infStr)
    end
    if ( kninf <> [] ) then
        str(kninf) = msprintf(fmtExcep,"-"+infStr)
    end
    if ( knan <> [] ) then
        str(knan) = msprintf(fmtExcep,nanStr)
    end
    kpos = find( xreg >= 0 )
    if ( kpos <> [] ) then
        str(kreg(kpos)) = msprintf(fmtPosFloats+"\n",xreg(kpos))
    end
    kneg = find( xreg < 0 )
    if ( kneg <> [] ) then
        str(kreg(kneg)) = msprintf(fmtNegFloats+"\n",xreg(kneg))
    end
    // Format the string back
    str = matrix(str,nr,nc)
endfunction

function argin = argindefault ( rhs , vararglist , ivar , default )
    // Returns the value of the input argument #ivar.
    // If this argument was not provided, or was equal to the 
    // empty matrix, returns the default value.
    if ( rhs < ivar ) then
        argin = default
    else
        if ( vararglist(ivar) <> [] ) then
            argin = vararglist(ivar)
        else
            argin = default
        end
    end
endfunction
// Returns the indices where 
//  * kpinf : x(kpinf) == +%inf, 
//  * kninf : x(kninf) == -%inf, 
//  * knan : x(knan) is a %nan 
//  * kreg : x(kreg) is not an infinity, not a nan
//  * xreg = x(kreg)
// These 4 sets of indices have no intersection.
//
// Example :
// x = [1 2 3 -%inf %inf %nan %inf %nan -%inf 4 5 6]
// [kpinf , kninf , knan , kreg , xreg] = infnanindices ( x )
// xreg = [1 2 3 4 5 6]
// kreg = [1 2 3 10 11 12]
// knan = [6 8]
// kninf = [4 9]
// kpinf = [5 7]
function [kpinf , kninf , knan , kreg , xreg] = infnanindices ( x )
  kpinf = find(x==%inf)
  kninf = find(x==-%inf)
  knan = find(isnan(x))
  kreg = find(abs(x)<>%inf & ~isnan(x))
  xreg = x(kreg)
endfunction


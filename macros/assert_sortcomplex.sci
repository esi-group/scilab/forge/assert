// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = assert_sortcomplex ( varargin )
  // Sort complex numbers in increasing order with a tolerance.
  // 
  // Calling Sequence
  //   y = assert_sortcomplex ( x )
  //   y = assert_sortcomplex ( x , reltol )
  //   y = assert_sortcomplex ( x , reltol , abstol )
  // 
  // Parameters
  //   x : a matrix of doubles, the values to be sorted
  //   reltol : a 1x1 matrix of doubles, the relative tolerance (default reltol=sqrt(%eps)).
  //   abstol : a 1x1 matrix of doubles, the absolute tolerance (default abstol=0).
  //   y : a matrix of doubles, the sorted matrix x
  // 
  // Description
  // This function combines assert_sort with assert_comparecomplex
  // in order to provide a sorting algorithm with numerical
  // comparison based on a mixed relative - absolute criteria.
  //
  // Compare first by real parts, then by imaginary parts.
  // Takes into account numerical accuracy issues, by using 
  // a mixed relative and absolute tolerance criteria.
  //
  // This function allows to take into account for the portability issues
  // related to the outputs of functions producing 
  // matrix of complex doubles.
  // If this algorithm is plugged into a sorting function, 
  // it allows to consistently produce a sorted matrix, 
  // where the order can be independent of the operating system,
  // the compiler or other forms of issues modifying the 
  // order (but not the values).
  //
  // Examples
  //   // Sort realistic data
  //  x = [
  //    -0.123452 - 0.123454 * %i
  //    -0.123451 + 0.123453 * %i
  //     0.123458 - 0.123459 * %i
  //     0.123456 + 0.123457 * %i
  //  ];
  //  // Permute randomly the array
  //  n = size(x,"*");
  //  grand("setsd",0);
  //  ip = grand(1,"prm",(1:n)');
  //  x = x(ip)
  //  // Consider less than 4 significant digits
  //  y = assert_sortcomplex(x,1.e-4)
  //
  // // Sort data from bug #415
  //  x = [
  //    -1.9914145
  //    -1.895889
  //    -1.6923826
  //    -1.4815461
  //    -1.1302576
  //    -0.5652256 - 0.0655080 * %i
  //    -0.5652256 + 0.0655080 * %i
  //    0.3354023 - 0.1602902 * %i
  //    0.3354023 + 0.1602902 * %i
  //     1.3468911
  //     1.5040136
  //     1.846668
  //     1.9736772
  //     1.9798866
  //  ];
  //  // Permute randomly the array
  //  n = size(x,"*");
  //  grand("setsd",0);
  //  ip = grand(1,"prm",(1:n)');
  //  x = x(ip)
  //  // Consider less than 4 significant digits
  //  y = assert_sortcomplex(x,1.e-5)
  //
  // Authors
  // Michael Baudin, DIGITEO, 2010
  //
  // Bibliography
  // http://gitweb.scilab.org/?p=scilab.git;a=blob;f=scilab/modules/polynomials/tests/nonreg_tests/bug_415.tst;h=0c716a3bed0dfb72c831972d19dbb0814dffde2b;hb=HEAD
  // http://gitweb.scilab.org/?p=scilab.git;a=blob_plain;f=scilab/modules/cacsd/tests/nonreg_tests/bug_68.tst;h=920d091d089b61bf961ea9e888b4d7d469942a14;hb=4ce3d4109dd752fce5f763be71ea639e09a12630
  
  [lhs,rhs]=argn()
  if ( and( rhs<>[1 2 3] ) ) then
    errmsg = sprintf("%s: Unexpected number of arguments : %d provided while %d to %d are expected.","assert_sortcomplex",rhs,1,3);
    error(errmsg)
  end
  //
  // Get the array x
  x = varargin(1)
  reltol = argindefault ( rhs , varargin , 2 , sqrt(%eps) )
  abstol = argindefault ( rhs , varargin , 3 , 0 )
  //
  // Check types
  if ( typeof(x)<>"constant" ) then
    errmsg = sprintf("%s: Unexpected type for x: got %s while constant is expected.","assert_sortcomplex",typeof(x));
    error(errmsg)
  end
  if ( typeof(reltol) <> "constant" ) then
    errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_sortcomplex" , 2 , "reltol" , typeof(reltol) , "constant" )
    error(errmsg)
  end  
  if ( typeof(abstol) <> "constant" ) then
    errmsg = sprintf ( gettext ( "%s: Unexpected type of input argument #%d : variable %s has type %s while %s is expected.") , "assert_sortcomplex" , 3 , "abstol" , typeof(abstol) , "constant" )
    error(errmsg)
  end
  //
  // Check sizes
  if ( size(reltol,"*") <> 1 ) then
    errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has size %d while %d is expected.") , "assert_sortcomplex" , 2 , "reltol" , size(reltol,"*") , 1 )
    error(errmsg)
  end
  if ( size(abstol,"*") <> 1 ) then
    errmsg = sprintf ( gettext ( "%s: Unexpected size of input argument #%d : variable %s has size %d while %d is expected.") , "assert_sortcomplex" , 3 , "abstol" , size(abstol,"*") , 1 )
    error(errmsg)
  end
  //
  // Check values of variables
  if ( reltol < 0 ) then
    errmsg = sprintf ( gettext ( "%s: Unexpected value input argument #%d : variable %s has negative entries.") , "assert_sortcomplex" , 2 , "reltol" )
    error(errmsg)
  end
  if ( abstol < 0 ) then
    errmsg = sprintf ( gettext ( "%s: Unexpected value input argument #%d : variable %s has negative entries.") , "assert_sortcomplex" , 3 , "abstol" )
    error(errmsg)
  end
  //
  compfun = list ( assert_comparecomplex , reltol , abstol )
  y = assert_sort ( x , [] , compfun )
endfunction
function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the 
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction


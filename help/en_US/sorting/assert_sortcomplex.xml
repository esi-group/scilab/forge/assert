<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from assert_sortcomplex.sci using help_from_sci().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="assert_sortcomplex" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>assert_sortcomplex</refname><refpurpose>Sort complex numbers in increasing order with a tolerance.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   y = assert_sortcomplex ( x )
   y = assert_sortcomplex ( x , reltol )
   y = assert_sortcomplex ( x , reltol , abstol )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> a matrix of doubles, the values to be sorted</para></listitem></varlistentry>
   <varlistentry><term>reltol :</term>
      <listitem><para> a 1x1 matrix of doubles, the relative tolerance (default reltol=sqrt(%eps)).</para></listitem></varlistentry>
   <varlistentry><term>abstol :</term>
      <listitem><para> a 1x1 matrix of doubles, the absolute tolerance (default abstol=0).</para></listitem></varlistentry>
   <varlistentry><term>y :</term>
      <listitem><para> a matrix of doubles, the sorted matrix x</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
This function combines assert_sort with assert_comparecomplex
in order to provide a sorting algorithm with numerical
comparison based on a mixed relative - absolute criteria.
   </para>
   <para>
Compare first by real parts, then by imaginary parts.
Takes into account numerical accuracy issues, by using
a mixed relative and absolute tolerance criteria.
   </para>
   <para>
This function allows to take into account for the portability issues
related to the outputs of functions producing
matrix of complex doubles.
If this algorithm is plugged into a sorting function,
it allows to consistently produce a sorted matrix,
where the order can be independent of the operating system,
the compiler or other forms of issues modifying the
order (but not the values).
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Sort realistic data
x = [
-0.123452 - 0.123454 * %i
-0.123451 + 0.123453 * %i
0.123458 - 0.123459 * %i
0.123456 + 0.123457 * %i
];
// Permute randomly the array
n = size(x,"*");
grand("setsd",0);
ip = grand(1,"prm",(1:n)');
x = x(ip)
// Consider less than 4 significant digits
y = assert_sortcomplex(x,1.e-4)

// Sort data from bug #415
x = [
-1.9914145
-1.895889
-1.6923826
-1.4815461
-1.1302576
-0.5652256 - 0.0655080 * %i
-0.5652256 + 0.0655080 * %i
0.3354023 - 0.1602902 * %i
0.3354023 + 0.1602902 * %i
1.3468911
1.5040136
1.846668
1.9736772
1.9798866
];
// Permute randomly the array
n = size(x,"*");
grand("setsd",0);
ip = grand(1,"prm",(1:n)');
x = x(ip)
// Consider less than 4 significant digits
y = assert_sortcomplex(x,1.e-5)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Michael Baudin, DIGITEO, 2010</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>http://gitweb.scilab.org/?p=scilab.git;a=blob;f=scilab/modules/polynomials/tests/nonreg_tests/bug_415.tst;h=0c716a3bed0dfb72c831972d19dbb0814dffde2b;hb=HEAD</para>
   <para>http://gitweb.scilab.org/?p=scilab.git;a=blob_plain;f=scilab/modules/cacsd/tests/nonreg_tests/bug_68.tst;h=920d091d089b61bf961ea9e888b4d7d469942a14;hb=4ce3d4109dd752fce5f763be71ea639e09a12630</para>
</refsection>
</refentry>

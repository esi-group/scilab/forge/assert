// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.

//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating assert\n");
helpdir = cwd;
funmat = [
  "assert_checkalmostequal"
  "assert_checkequal"
  "assert_checktrue"
  "assert_checkfalse"
  "assert_checkfilesequal"
  "assert_checkerror"
  "assert_generror"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "assert";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
// Generate the sorting help
mprintf("Updating assert/sorting\n");
helpdir = fullfile(cwd,"sorting");
funmat = [
  "assert_sort"
  "assert_compare"
  "assert_comparecomplex"
  "assert_sortcomplex"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "assert";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
// Generate the support help
mprintf("Updating assert/support\n");
helpdir = fullfile(cwd,"support");
funmat = [
  "assert_computedigits"
  "assert_cond2reltol"
  "assert_cond2reqdigits"
  "assert_csvwrite"
  "assert_csvread"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "assert";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

